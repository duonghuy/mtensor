This is repository of dense subtensor detection work.

Citing this work: If you want to refer this work in a publication, please cite the following paper from ICDE 2020:

Q. H. Duong, H. Ramampiaro, and K. Nørvåg, (2020), "Multiple Dense Subtensor Estimation with High Density 
Guarantee". In Proceedings of 36th IEEE International Conference on Data Engineering (ICDE 2020). IEEE.

BibTex:
```
@inproceedings{Duong2020ICDE,
	author    = {Quang{-}Huy Duong and Heri Ramampiaro and Kjetil N{\o}rv{\aa}g},
	title = {Multiple Dense Subtensor Estimation with High Density Guarantee},
	booktitle  = {Proceedings of the 36th IEEE International Conference on Data Engineering, ICDE},
	year = {2020},
	pages     = {637--648},
	publisher = {{IEEE}}
}
```

